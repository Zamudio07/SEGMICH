#SEGMICH
<p>
El producto a desarrollar es el sistema SEGMICH (Seguimiento de Michoacán en redes sociales), una aplicación de software diseñada para el monitoreo y análisis de las redes sociales de las entidades gubernamentales del estado de Michoacán.
</p>
#Tecnologías
<p>
Las tecnologías que se usarán para realizar el sistema se listan a continuación:
</p>
- [React](https://es.react.dev/ "React") en la versión **18.2.0**
- [Node.js](https://nodejs.org/en/download "Node.js") en la versión **20.12.2**
-[Python](https://www.python.org/downloads/ " Python") en la versión **3.12.3**
- [MySQL Workbench](https://dev.mysql.com/downloads/workbench/ "MySQL Workbench") en la versión **8.0.36**
- [Tailwind css](https://tailwindcss.com/docs/installation "Tailwind css") en la versión  **3.'.2**
- Api ....

Además de algunas librerías como:
- [react-router-dom](https://reactrouter.com/en/main/start/tutorial "react-router-dom") en la versión **6.22.3**